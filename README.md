Readme

## Django headless blueprint   
### Description:
- Base to bootstrap a new project with Django, DRF and some useful extra bits. 
   
### Project structure:
- Docker / docker-compose
- Gunicorn
- Pipenv to manage virtual environment
- dotenv for control dev/prod environment
- Separated dev / prod settings, driven by .env
- SECRET_KEY really secret
- 'make' to make our life easy
- Utility shell scripts
- iPython / shell_plus
- Already a basic 'main' app in place
- Some loaded sample data
- Grapelli to make Admin even better
- DRF API
- cors-headers to allow local access to API
- Test by PyTest 
- Basic HTML / jQuery client
- Administrative area renamed from standard url '/admin' to '/manage'.

### Setting up
- Clone the project locally and setup the upstream to the new project repository.
- Create a `.env` file from `dot_env_sample` and if needed, tailor it to your needs.
    - If you change it - no need to - update project name 'project' to the name of your project at `Makefile`, `docker-compose.yml` and where required.
- Run `make start` to see something happening
- Run `make help` to see what more it can do for you.  
- Alternatively we can run `docker-compose` commands directly:
    - `docker-compose up --build -d` 
    - `docker-compose up -d` 
    - `docker-compose stop` 
    - `scripts/restart_project.sh [build]`
- To run Django 'manage.py' commands:
    - `docker-compose run project python3 project/manage.py {command}` 
- To access shell_plus and ipython - same as `make djangoshell`:
    - `docker-compose run project python3 project/manage.py shell_plus --ipython` 
 
### Running:
 - `make start` and you will get site available in `localhost:8000` - or other `WSGI_PORT` value.  
    - This will start `docker-compose` in `attached` mode.
    - To stop: 
        - `ctrl-C` 
        - or `make stop` in other window.
        - or ` ctrl-z` in same window and `make stop`.
 - `make startbg` - start project in background - detach.
- `make startclient` - starts a minimal HTML API client on standard port `8002`. 
 
### Accessing:
 - `localhost:8000` - Standard Django 'view/template' page.  
 - After running `make startclient` we will be able to access at `localhost:8002` - a minimal HTML / jQuery Client to consume Django Project API endpoints.
 - After running `make loaddata` we will be able to access the administration area at '/manage' with 'admin/superuser'.
   
-----
